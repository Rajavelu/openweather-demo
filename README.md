# README #

### Description ###

A simple iOS app for weather forecasting. It basically shows actual weather for your location. In the Forecast tab it shows the forecast for next 7 days at current location.

### Device support ###

* Full support for all iPhone screen sizes
* Portrait orientation

### Libraries & Dependencies ###

* Firebase
* Alamofire
* SwiftyJSON
* IJReachability
* CoreLocation