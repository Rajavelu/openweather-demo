//
//  Weather.swift
//  WeatherApp
//
//  Created by Rajavelu Chandrasekaran on 05/08/15.
//  Copyright 2015 STRV. All rights reserved.
//

import UIKit

class Weather: NSObject {
   
    var city:String?
    var country:String?
    var condition:String?
    var temperature:String?
    var humidity:String?
    var rain:String?
    var pressure:String?
    var windSpeed:String?
    var windDirection:String?
    var day:String?    
}
