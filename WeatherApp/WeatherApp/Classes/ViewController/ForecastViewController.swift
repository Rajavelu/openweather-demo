//
//  ForecastViewController.swift
//  WeatherApp
//
//  Created by Rajavelu Chandrasekaran on 04/08/15.
//  Copyright 2015 STRV. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController, UITableViewDataSource,UITableViewDelegate  {
    
    @IBOutlet weak var forecastTable: UITableView!
    @IBOutlet weak var cityLabel: UILabel!
    
    var tableData:NSMutableArray!
    
    // MARK: - View Life Cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        // Do any additional setup after the view is rendered, typically from a nib.
        
        tableData = NSMutableArray()
        
        if let latitude = lat {
            cityLabel.text = city
            
            // Checking for internet connectivity
            if (IJReachability.isConnectedToNetwork()) {
                // Getting weather forecast
                self.getWeatherForecast()
            } else {
                var alert = UIAlertView(title: "No Internet Connection", message: "Please connect to internet and try again", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if let tableData = self.tableData {
            return self.tableData.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("forecastcell", forIndexPath: indexPath) as! ForecastTableViewCell
    
    // Configure the cell...
        var weatherObj = self.tableData[indexPath.row] as! Weather
        
        cell.weatherImage.image = UIImage(named: "Sun_Big")
        cell.day.text = weatherObj.day
        cell.condition.text = weatherObj.condition
        cell.temperature.text = "\(weatherObj.temperature!)°"
    
        return cell
    }

    
    // MARK: - API Call and Response parser
    
    // This method gets weather forecast
    func getWeatherForecast () {
        
        //var forecastURL = "http://api.openweathermap.org/data/2.5/forecast/daily?q=prague&cnt=8&units=metric"
        var forecastURL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(lat!)&lon=\(long!)&cnt=8&units=metric"
        
        request(.GET, forecastURL ).responseJSON() {
            (_, response, data, error) in
            
            if (response?.statusCode == 200) {
                var jsonValue = JSON(data!)
                self.parseWeatherForecastData(jsonValue)
            }
            else {
                var alert = UIAlertView(title: "There is a problem in getting the data", message:error?.localizedDescription, delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }           
        }
    }
    
    // This method parses the response and converts it to an array of weather model
    func  parseWeatherForecastData(data: JSON) {
        
        var forecastList = data["list"]
        for var index = 1; index < forecastList.count; ++index {
            
            var weatherData = forecastList[index]
            var dayForecast = Weather()
            
            var temperatureDict = weatherData["temp"]
            var temp:Float = temperatureDict["day"].floatValue
            dayForecast.temperature = (NSString(format: "%.0f", temp)) as String
            
            var weatherArray = weatherData["weather"]
            var weatherElement = weatherArray[0]
            dayForecast.condition = weatherElement["main"].stringValue
            dayForecast.day = self.getDayOfWeek((weatherData["dt"].doubleValue))
            
            self.tableData.addObject(dayForecast)
        }
        
        forecastTable.reloadData()
    }
    
    
    // MARK: - Utility methods
    
    // This method finds the day of the week for a given timestamp
    func getDayOfWeek(timeStamp:Double)-> String? {
        
        var date = NSDate(timeIntervalSince1970: (NSTimeInterval)(timeStamp))
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var dateString = dateFormatter.stringFromDate(date)
        
        var weekDay:Int
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.dateFromString(dateString) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let myComponents = myCalendar.components(.CalendarUnitWeekday, fromDate: todayDate)
            weekDay = myComponents.weekday
            
            var day = weekDay
            switch day {
            case 1:
                return "Sunday"
            case 2:
                return "Monday"
            case 3:
                return "Tuesday"
            case 4:
                return "Wednesday"
            case 5:
                return "Thursday"
            case 6:
                return "Friday"
            case 7:
                return "Saturday"
            default:
                println("This shouldn't happen")
            }

        } else {
            println("bad input")
            return ""
        }
        
        return nil
    }
    
}

