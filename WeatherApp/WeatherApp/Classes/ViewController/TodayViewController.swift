//
//  TodayViewController.swift
//  WeatherApp
//
//  Created by Rajavelu Chandrasekaran on 04/08/15.
//  Copyright 2015 STRV. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase

let K_HEIGHT_IPHONE4S:CGFloat = 480.0
let K_HEIGHT_IPHONE5:CGFloat = 568.0

public var lat:CLLocationDegrees?
public var long:CLLocationDegrees?
public var city:String?

class TodayViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    
    //Constraint Reference
    @IBOutlet weak var topViewHgtConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHgtConstraint: NSLayoutConstraint!
    @IBOutlet weak var weatherImgHgtConstraint: NSLayoutConstraint!
    @IBOutlet weak var shareBtnBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var windObjectBottomConstraint: NSLayoutConstraint!
    
    var locationManager: CLLocationManager!
    
    
    // MARK: - View Life Cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setupContraints()
        
        self.topView.hidden=true
        self.bottomView.hidden=true
        self.shareButton.hidden=true
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        // Do any additional setup after the view is rendered, typically from a nib.
        
        // Checking for internet connectivity
        if (IJReachability.isConnectedToNetwork()) {
            self.getCurrentLocation()
        } else {
            var alert = UIAlertView(title: "No Internet Connection", message: "Please connect to internet and try again", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Constraints setup
    
    func setupContraints () {
        if((UIScreen.mainScreen().bounds.height==K_HEIGHT_IPHONE4S) || (UIScreen.mainScreen().bounds.height==K_HEIGHT_IPHONE5)) {
            
            weatherImgHgtConstraint.constant = 45;
            topViewHgtConstraint.constant = 140;
            bottomViewHgtConstraint.constant = 140;
            windObjectBottomConstraint.constant = 20;
            shareBtnBottomConstraint.constant = 5;
        }
    }
    
    
    // MARK: - CoreLocation Methods and Delegates
    
    // This methods gets device's current location
    func getCurrentLocation () {
        
        // Initializing Location manager and requesting location
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    // This method gets called once the device's location is retreived
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        var userLocation:CLLocation = locations[0] as! CLLocation
        long = userLocation.coordinate.longitude;
        lat = userLocation.coordinate.latitude;
        
        // Get today's weather
        self.getTodayWeather()
    }
    
    
    // MARK: - API Call and Response parser
    
    // This method gets today's weather
    func getTodayWeather () {
   
        //var weatherURL = "http://api.openweathermap.org/data/2.5/weather?q=prague&units=metric"
        var weatherURL = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat!)&lon=\(long!)&units=metric"
        
        request(.GET, weatherURL ).responseJSON() {
            (_, response, data, error) in
            
            if (response?.statusCode == 200) {
                var jsonValue = JSON(data!)
                var weather:Weather = self.parseTodayWeatherData(jsonValue)
                
                self.updateTodayUI(weather)
                self.updateFireBase(weather)
            }
            else {
                var alert = UIAlertView(title: "There is a problem in getting the data", message:error?.localizedDescription, delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        }
    }
    
    // This method parses the response and converts it to a weather model
    func  parseTodayWeatherData(data: JSON) -> Weather {
       
        var weatherObj = Weather()
        
        var mainDict = data["main"]
        var temp:Float = mainDict["temp"].floatValue
        weatherObj.temperature = (NSString(format: "%.0f", temp)) as String
        
        var rainDict = data["rain"]
        weatherObj.rain = rainDict["3h"].stringValue
        
        var windDict = data["wind"]
        var speed:Float = windDict["speed"].floatValue
        speed = speed * 3.6
        weatherObj.windSpeed = speed.description
        weatherObj.windDirection = windDict["deg"].stringValue
        
        var weatherArray = data["weather"]
        var weatherElement = weatherArray[0]
        weatherObj.condition = weatherElement["main"].stringValue
        
        city = data["name"].stringValue
        weatherObj.city = data["name"].stringValue
        weatherObj.humidity = mainDict["humidity"].stringValue
        
        var pressure:Float = mainDict["pressure"].floatValue
        weatherObj.pressure = (NSString(format: "%.0f", pressure)) as String
    
        return weatherObj
    }
    
    // This method updates the UI with created weather model
    func updateTodayUI(weather:Weather) {
        
        self.topView.hidden=false
        self.bottomView.hidden=false
        self.shareButton.hidden=false
        
        cityLabel.text = weather.city
        temperatureLabel.text = "\(weather.temperature!)°C"
        conditionLabel.text = weather.condition
        humidityLabel.text = "\(weather.humidity!)%"
        pressureLabel.text = "\(weather.pressure!) hPa"
        windSpeedLabel.text = "\(weather.windSpeed!) km/h"
        windDirectionLabel.text = weather.windDirection
        
        if weather.rain=="" {
            rainLabel.text =  "NA"
        }else {
            rainLabel.text =  "\(weather.rain!) mm"
        }
    }
    
    // MARK: - Firebase Methods
    
    // Creating a reference to a Firebase and storing data
    func updateFireBase(weather:Weather) {
     
        var latitude:String = "\(lat!)"
        var longitude:String = "\(long!)"
        var location:String = weather.city!
        var temperature:String = "\(weather.temperature!)°C"
        
        var weatherData = ["Latitude": latitude, "Longitude": longitude,
                            "Location": location, "Temperature": temperature]
        
        var rootFireBaseRef = Firebase(url:"https://amber-inferno-1587.firebaseio.com/")
        var childRef = rootFireBaseRef.childByAppendingPath("weather")
        childRef.setValue(weatherData)
        
    }
    
    // MARK: - Button Actions
    
    @IBAction func onTapShareButton(sender: AnyObject) {
        let textToShare = "STRV is awesome!  Check out our website!"
        
        if let myWebsite = NSURL(string: "http://www.strv.com/")
        {
            let objectsToShare = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }

}

