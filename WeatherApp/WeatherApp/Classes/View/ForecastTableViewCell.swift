//
//  ForecastTableViewCell.swift
//  WeatherApp
//
//  Created by Rajavelu Chandrasekaran on 05/08/15.
//  Copyright 2015 STRV. All rights reserved.
//

import UIKit

public class ForecastTableViewCell: UITableViewCell {
	@IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var condition: UILabel!
    @IBOutlet weak var temperature: UILabel!
}